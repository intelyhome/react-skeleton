const authActionTypes = {
  setSignInResponse: 'auth.setSignInResponse',
  setSignInError: 'auth.setSignInError'
};

const userActionTypes = {
  setSignUpResponse: 'user.setSignUpResponse',
  setSignUpError: 'auth.setSignUpError'
};

export default {
  ...authActionTypes,
  ...userActionTypes
};
