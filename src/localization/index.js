import i18n from "i18next";
import detectBrowserLanguage from "detect-browser-language";
import settings from "../config/settings";
import {I18nextProvider} from "react-i18next";
import enLocalizations from './en';
import huLocalizations from './hu';

const browserLanguage = detectBrowserLanguage().substring(0, 2) || 'en';

if (!localStorage.getItem('Language')) {
  localStorage.setItem(
    'Language',
    browserLanguage
  );
}

const getLng = () => localStorage.getItem('Language') || browserLanguage || settings.Localize.defaultLanguageCode;


i18n
  //.use(XHRRequest)
  .use(I18nextProvider)
  .init({
    lng: getLng(),
    fallbackLng: 'en',
    debug: true,
    defaultNS: ['main'],
    ns: ['main'],
    fallbackNS: 'main',
    interpolation: {
      escapeValue: false // react already safes from xss
    },
    missingKeyHandler: (lng, ns, key, fallbackValue) => {
      window.debug.log('i18n missing key');
      window.debug.table({"Language": lng, "NameSpace": ns, "Key": key, "Fallback value": fallbackValue});

    },
    resources: {
      en: enLocalizations,
      hu: huLocalizations
    }
  });

export const setLanguage = (language = "en") => {
  i18n.changeLanguage(language).then(() => localStorage.setItem('Language', language));
};

export const changeLanguage = event => {
  event.preventDefault();
  setLanguage(event.target.getAttribute('data-lang'));
  return false
};

export default i18n;
