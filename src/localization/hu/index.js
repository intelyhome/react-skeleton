export default Object.assign(
  require("./main"),
  require("./common"),
  // custom language kits
  require("./uikit"),
  require("./sidebar")
);
