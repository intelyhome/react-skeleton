import PropTypes from "prop-types";
import React from "react";
import {pages} from '../config/pages';
import Layout from "./layouts";
import App from "./App";
import {Redirect, Route, Switch} from "react-router-dom";
/**
 * Dynamic page router from config/pages
 * @file Use this component to render page routes from config/pages
 * @module Router
 * @extends React.Component
 */

/**
 * Use this component to render page routes from config/pages
 * @param {Node} children - React or DOM node
 * @return {React.Component} Layout
 * @constructor
 */
class Router extends React.Component {

  /**
   * Generate route
   * @param {String} page - Key of page
   * @return {React.Component} Route
   */
  static generateRoute(page) {
    const pageData = pages[page];
    const renderMethod = Layout.renderWithLayout(page, pageData.render || pageData.component, pageData.layout || undefined);
    return <Route exact={pageData.exact || false} key={page} path={pageData.link}
                  render={() => (
                    pageData.authenticationRequired &&
                    !App.isLoggedIn()
                  ) ? <Redirect to={pages.Login.link}/> : renderMethod
                  }/>
  }

  /**
   * Render the component
   * @return {React.Component} Layout
   */
  render() {
    return <Switch>{Object.keys(pages).map(pageKey => Router.generateRoute(pageKey))}</Switch>;
  }
}

Router.propTypes = {
  children: PropTypes.element,
  user: PropTypes.object
};

export default Router;
