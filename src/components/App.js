import React, {Suspense} from 'react';
import {ConnectedRouter} from 'connected-react-router';
import {Provider} from 'react-redux';
import PropTypes from 'prop-types';
import Swagger from 'swagger-client';
import detectBrowserLanguage from 'detect-browser-language';
import Router from './Router';
import settings from "../config/settings";
import 'semantic-ui-css/semantic.min.css';
import {hot} from "react-hot-loader";
import Observables from "../utils/observables";
import './App.scss';
import {I18nextProvider} from 'react-i18next';
import Localization from "../localization";
import LoaderIndex from "./modules/loader";
import Layout from "./layouts";

/**
 * React App component
 * @file This component is your React App home
 * @module App
 * @extends React.Component
 */

/**
 * Use this component to render page routes from config/pages
 * @param {Object} store - Configured Redux store
 * @param {Object} history - React router generated browser-history
 * @return {React.Component} Provider
 * @constructor
 */
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      swaggerClientLoaded: false,
      renderMathRandom: Math.random()
    };

    /**
     * Register an observable called *'reloadApp'*
     * @see {@link module:Observables}
     * @event reloadApp
     */
    Observables.register('reloadApp', () => this.setState({renderMathRandom: Math.random()}))
  }

  /**
   * Change the App language
   * @param {String} languageCode - The language iso standard code to set
   * @static
   */
  static setLanguage(languageCode = settings.Localize.defaultLanguageCode) {
    return localStorage.setItem('Language', languageCode);
  }

  /**
   * Get the current language
   * @return {String}
   * @static
   */
  static getLanguage() {
    return localStorage.getItem('Language') || settings.Localize.defaultLanguageCode;
  }

  /**
   * Current user is logged in or not
   * @return {Boolean}
   * @static
   */
  static isLoggedIn() {
    return !!localStorage.getItem(settings.API.headers.Authentication.localStorageKey);
  }

  /**
   * Get and set the browser's language to localStorage
   * @todo: Set languages from a fix array
   * @methodOf module:App
   */
  setBrowserLanguage() {
    if (!localStorage.getItem('Language')) {
      const setLanguageCode = detectBrowserLanguage().substring(0, 2) || settings.Localize.defaultLanguageCode;
      localStorage.setItem('Language', setLanguageCode);
    }
  }

  /**
   * Get the authentication token from localStorage
   * @return {Boolean|String} AccessToken - With Bearer prefix
   */
  getAuthenticationToken() {
    if (!localStorage.getItem(settings.API.headers.Authentication.localStorageKey))
      return false;
    return settings.API.headers.Authentication.prefix + localStorage.getItem(settings.API.headers.Authentication.localStorageKey);
  }

  /**
   * Mount the component
   * @async
   * @return {Promise}
   */
  async componentDidMount() {
    this.setBrowserLanguage();
    await Swagger({
      url: settings.API.host,
      requestInterceptor: request => {

        request.headers['Content-Type'] = 'application/json';
        request.headers['Accept-Language'] = App.getLanguage();

        if (App.isLoggedIn())
          request.headers[settings.API.headers.Authentication.name] = this.getAuthenticationToken();

        return request;

      }
    })
      .then(swaggerClient => {
        window.debug.log("Swagger client loaded: ", swaggerClient);
        window.swaggerClient = swaggerClient;
        this.setState({swaggerClientLoaded: true})
      }).catch(error => {
        window.debug.error("Swagger client load error: ", error);
        return error;
      });
  }

  /**
   * Render the component
   * @return {React.Component} - Provider
   */
  render() {
    const {store, history} = this.props;
    const {swaggerClientLoaded} = this.state;

    return (
      <Provider store={store}>
        {swaggerClientLoaded &&
        <ConnectedRouter history={history}>
          <Suspense fallback={<LoaderIndex/>}>
            <I18nextProvider i18n={Localization}>
              <Layout>
                <Router/>
              </Layout>
            </I18nextProvider>
          </Suspense>
        </ConnectedRouter>}
      </Provider>
    );
  }
}

App.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};
export default hot(module)(App);
