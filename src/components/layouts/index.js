import PropTypes from "prop-types";
import React, {Component} from "react";

import Loadable from "react-loadable";
import Loader from '../../components/modules/loader';

const ErrorBoundary = Loadable({
  loader: () => import('../../components/modules/errorBoundary' /* webpackChunkName: "errorboundary" */),
  loading: Loader
});


class Layout extends Component {

  /**
   * Render the VirtualComponent (can be HOOK or Class based component)
   * @param {String} page - Name of the page (need only for the Layout Key
   * @param {React.Component|Function} VirtualComponent - Valid component or HOOK based arrow function
   * @param {React.Component|Function|undefined} VirtualLayout - Valid component or HOOK based arrow function or undefined if no layout specified (like at a redirect route)
   * @return {React.Component} VirtualLayout rendered with VirtualComponent child (passed from Router component)
   */
  static renderWithLayout = (page = '', VirtualComponent = <Loader/>, VirtualLayout = undefined) => {
    if(!VirtualLayout) return <VirtualComponent/>;
    return <VirtualLayout key={'forPage-' + page}>
      <VirtualComponent/>
    </VirtualLayout>;
  };

  componentDidMount() {
    document.getElementById('PWAGoToDashboardButton').className = 'animated';
    document.getElementById('PWALoader').remove();
  }

  render() {
    const {children} = this.props;
    return <ErrorBoundary key={'LayoutErrorBoundary'}>
      {children}
    </ErrorBoundary>;
  }
}

Layout.propTypes = {
  children: PropTypes.element,
  user: PropTypes.object
};

export default Layout;
