/**
 * @file MainLayoutNavigation, this component is used to display a navigation in main layout
 *
 * @module MainLayoutNavigation
 * @extends Component
 */
import PropTypes from "prop-types";
import React from 'react'
import {Icon, Image, Menu, Container} from 'semantic-ui-react'
import './Navigation.scss';
import BigLogo from '../../../../assets/logos/big.png';
import {Link} from "react-router-dom";

class MainLayoutNavigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: true,
      activeItem: '/'
    };
  }

  /**
   * headerLogo
   * @method
   * @summary Render the header logo image
   * @return {Node} React node containing not-found view
   */
  headerLogo() {
    return <Menu.Header className={this.state.expanded ? 'logo cursor-pointer expanded' : 'logo cursor-pointer'} as={'a'} link onClick={()=>window.location.href = '/'}>
      <Image src={BigLogo} size='tiny'/>
    </Menu.Header>;
  }

  handleItemClick = (e, {link}) => this.setState({activeItem: link});

  render() {
    const {activeItem} = this.state;
    const menus = [];

    return <Menu fixed='top' inverted floated={'right'} className={'navigation-bar'}>
      <Container>
        {this.headerLogo()}
        {menus.map(menuItem => <Menu.Item
          as={'div'}
          key={menuItem.title}
          link
          active={activeItem === menuItem.link}
          onClick={this.handleItemClick}
        >
          <Link className={'menu-link'} to={menuItem.link}>{menuItem.title}</Link>
        </Menu.Item>)}
        <Menu.Item
          as={'div'}
          key={'login'}
          link
        >
          <Link className={'menu-link'} to={'/login'}><Icon name={'bars'} style={{fontSize: '2rem', marginTop: '-10px'}}/></Link>
        </Menu.Item>
      </Container>
    </Menu>;
  }
}

MainLayoutNavigation.propTypes = {
  children: PropTypes.any
};

export default MainLayoutNavigation;
