/**
 * @file ContentHeaderBar, this component is used to display header at the top of the content
 *
 * @module ContentHeaderBar
 * @extends Component
 */
import PropTypes from "prop-types";
import React from 'react'
import './ContentHeaderBar.scss';
import {Icon, Search} from "semantic-ui-react";


class ContentHeaderBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: true
    };
  }

  renderTopBarCollapseIcon() {
    return <div className={'topbar-collapse-icon pull-left'}><Icon link name='bars' size={'big'}/></div>
  }

  renderTopBarSearch() {
    return <div className={'topbar-search-wrapper pull-right'}>
      <Search
        fluid
      />
    </div>
  }


  render() {
    return <div className={'content-header-bar'}>
      {this.renderTopBarSearch()}
      {this.renderTopBarCollapseIcon()}
      {this.props.children}
    </div>;
  }
}

ContentHeaderBar.propTypes = {
  children: PropTypes.any
};

export default ContentHeaderBar;
