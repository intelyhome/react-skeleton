import PropTypes from "prop-types";
import React from "react";
import "./Layout.scss";
import MainLayoutNavigation from "./components/Navigation";

class LandingLayout extends React.Component {
  render() {
    return (<div className={"landing layout"}>
        <div className={'inner-wrap'}>
          <div className="wrap-fluid">
            <MainLayoutNavigation/>
            <div className={'container-wrapper'}>
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

LandingLayout.propTypes = {
  children: PropTypes.any
};

export default LandingLayout;
