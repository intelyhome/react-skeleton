import PropTypes from "prop-types";
import React from "react";
import {Grid, Image} from "semantic-ui-react";
import "./Layout.scss";
import RocketGraphic from "../../../assets/vectors/login-rocket-smoke.svg"

class AuthenticationLayout extends React.Component {
  render() {
    return (<div className={"authentication layout"}>
        <div className={'inner-wrap'}>
          <div className="wrap-fluid">
            <Grid columns={2} divided stretched>
              <Grid.Column only='computer'>
                <h2 className={"logo-text"}>
                  Drukka
                  Digitals
                </h2>
              </Grid.Column>
              <Grid.Column className={'authentication-form'}>
                {this.props.children}
                <Image className="rocket-svg" alt={"Drukka Digitals rocket"} src={RocketGraphic}/>
              </Grid.Column>
            </Grid>
          </div>
        </div>
      </div>
    );
  }
}

AuthenticationLayout.propTypes = {
  children: PropTypes.any
};

export default AuthenticationLayout;
