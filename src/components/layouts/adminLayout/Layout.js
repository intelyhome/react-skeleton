import PropTypes from "prop-types";
import React from "react";
import AdminLayoutSidebar from "./components/Sidebar";
import {Container} from "semantic-ui-react";

import './Layout.scss';
import ContentHeaderBar from "./components/ContentHeaderBar";


class AdminLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hotLoader: false,
      sidebarFixed: true
    };
  }

  render() {
    return (<AdminLayoutSidebar sidebarFixed={this.state.sidebarFixed}>
        <ContentHeaderBar setSidebarFixed={()=>this.setState({sidebarFixed: !this.state.sidebarFixed})}/>
        <Container fluid className={'layout-main-wrapper'}>{this.props.children}</Container>
      </AdminLayoutSidebar>
    );
  }
}

AdminLayout.propTypes = {
  children: PropTypes.any
};

export default AdminLayout;
