import PropTypes from "prop-types";
import React, {createRef} from 'react'
import {Container, Flag, Grid, Header, Icon, Image, Menu, Sidebar} from 'semantic-ui-react'
import './Sidebar.scss';
import {Link} from "react-router-dom";
import {changeLanguage} from "../../../../localization";
import {withTranslation} from "react-i18next";
//import SmallLogo from '../../../../assets/logos/small.png';
//import BigLogo from '../../../../assets/logos/big.png';
/**
 * @file AdminLayoutSidebar, this component is used to display a sidebar in admin layout
 *
 * @module AdminLayoutSidebar
 * @extends React.Component
 */
class AdminLayoutSidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: true,
      sidebarFixed: this.props.sidebarFixed
    };

    this.contextRef = createRef();
  }


  /**
   * headerLogo
   * @method
   * @summary Render the header logo image
   * @return {Node} React node containing not-found view
   */
  headerLogo() {
    this.footerBox();
    return <Menu.Header
      className={this.props.sidebarFixed || this.state.expanded ? 'sidebar-header expanded' : 'sidebar-header'}>
      {/*<Image src={this.state.expanded ? BigLogo : SmallLogo} size='small'/>*/}
      {<h2>{this.props.sidebarFixed || this.state.expanded ? "DrukkaDigitals.com" : "DD"}</h2>}
    </Menu.Header>;
  }

  /**
   * footerBox
   * @method
   * @summary Render the footer box to sidebar
   * @return {Node} React node containing not-found view
   */
  footerBox() {
    const {t} = this.props;

    return <Grid.Row verticalAlign={'middle'} className={this.props.sidebarFixed || this.state.expanded ? 'sidebar-footer expanded' : 'sidebar-footer'}>
      <Grid.Column className={'text-center'}>
        <Header>
          <Image
            as={'img'}
            circular
            centered
            bordered
            className={'user-profilephoto'}
            src={'https://comps.canstockphoto.com/astronaut-in-moon-earth-planet-rocket-vector-clipart_csp45979907.jpg'}
            wrapped
          />
          <br/>
          <Header as='h3' content={t('sidebar:footer.welcome', {firstName: 'Ferenc'})}/>
          <Flag className={'cursor-pointer'} data-lang={'en'} onClick={changeLanguage} name={'us'}/>
          <Flag className={'cursor-pointer'} data-lang={'hu'} onClick={changeLanguage} name={'hu'}/>
        </Header>

      </Grid.Column>
    </Grid.Row>;
  }

  render() {
    return <Sidebar.Pushable as={Container} fluid className={!this.state.expanded && !this.props.sidebarFixed ? 'closed' : ''}>
      <Sidebar
        className={'sidebar-component'}
        as={Menu}
        animation='push'
        inverted
        borderless={true}
        vertical
        visible={true}
        size={'massive'}
        width={this.props.sidebarFixed || this.state.expanded ? 'wide' : 'very thin'}
        onMouseOver={() => !this.props.sidebarFixed && this.setState({expanded: true})}
        onMouseLeave={() => !this.props.sidebarFixed && this.setState({expanded: false})}
      >

        {this.headerLogo()}

        <Menu.Item as={Link} to={'/dashboard/home'} className={'menu-link'}>
          <Icon name='home' size={'large'}/>
          <span className={this.state.expanded ? 'menu-label visible' : 'menu-label hidden'}>Home</span>
        </Menu.Item>

        <Menu.Item as={Link} to={'/dashboard/ui-kit/table'} className={'menu-link'}>
          <Icon name='calendar outline' size={'large'}/>
          <span className={this.state.expanded ? 'menu-label visible' : 'menu-label hidden'}>UIKit / Table</span>
        </Menu.Item>
        <Menu.Item as={Link} to={'/dashboard/ui-kit/form'} className={'menu-link'}>
          <Icon name='columns' size={'large'}/>
          <span className={this.state.expanded ? 'menu-label visible' : 'menu-label hidden'}>UIKit / Form</span>
        </Menu.Item>

        {this.footerBox()}
      </Sidebar>
      <Sidebar.Pusher dimmed={false}>
        {this.props.children}
      </Sidebar.Pusher>
  </Sidebar.Pushable>;
  }
}

AdminLayoutSidebar.propTypes = {
  children: PropTypes.any,
  sidebarFixed: PropTypes.bool,
  t: PropTypes.func
};

export default withTranslation()(AdminLayoutSidebar);
