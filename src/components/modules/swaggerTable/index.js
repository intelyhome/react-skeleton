import React, {useEffect, useState} from 'react';
import PropTypes from "prop-types";
import {Loader, Table} from "semantic-ui-react";
import RequestError from "../requestError";
import * as tables from "../../../utils/tables";
/**
 * Dynamic Swagger based table component
 * @file Use this component with required properties for render advanced tables from swagger paths
 * @module SwaggerTable
 */

/**
 * Use this component with required properties for render advanced tables from swagger endpoint
 * @param {Function|Boolean} request - Swagger request function (Or networking layer request function)
 * @param {Array} data - Give a data array
 * @param {Object} columns - Array of table headers
 * @param {Array|Boolean} controls - Array of control items
 * @return {React.Component} Table
 * @constructor
 */
const SwaggerTable = ({request = false, data = [], columns = {}, controls = false}) => {
  const [loadedData, setLoadedData] = useState(undefined);
  const [error, setError] = useState(false);

  /**
   * loadData - Fetch data from API
   * @return {Promise<any>}
   */
  const loadData = () => request()
    .then(setLoadedData)
    .catch(setError);


  useEffect(() => {
    if (request) loadData();
    if (!request) setLoadedData(data);

  }, [window.swaggerClient, request, data]);


  if (error) return <RequestError error={error}/>;
  if (!loadedData || !request && data.length === 0) return <Loader active={true}/>;

  return <Table striped selectable>
    <Table.Header>
      {tables.renderTableHeaderRow(columns, controls)}
    </Table.Header>

    <Table.Body>
      {tables.renderTableBodyRows(columns, loadedData, controls)}
    </Table.Body>
  </Table>;
};

SwaggerTable.propTypes = {
  columns: PropTypes.object.isRequired,
  request: PropTypes.func,
  data: PropTypes.array,
  controls: PropTypes.array
};

export default SwaggerTable;
