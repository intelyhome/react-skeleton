/**
 * Basic loader component
 * @file Use this component with 'active=true' property to show a fancy loader
 * @module LoaderIndex
 */
import React from 'react';
import {Loader} from "semantic-ui-react";
import PropTypes from "prop-types";

const LoaderIndex = ({active = true}) => {
  return <Loader inverted content='Loading' active={active}/>;
};

LoaderIndex.propTypes = {
  active: PropTypes.bool
};

export default LoaderIndex;
