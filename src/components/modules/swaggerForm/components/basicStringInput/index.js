import React from 'react';
import PropTypes from "prop-types";
import {Input} from "semantic-ui-react";
/**
 * BasicStringInput component
 * @file This component render the basic string inputs like text, email, password etc.
 */

/**
 *
 * @param {Object} fieldData - The field data from Swagger spec
 * @param {String} field - Key of the field
 * @returns {React.Component} - Input
 * @memberOf module:SwaggerForm
 * @constructor
 */
const BasicStringInput = ({fieldData = {}, field = ''}) => {
  /**
   * Get the input type if the format specified
   * @return {'string'|'email'|'password'}
   */
  const getInputType = () => {
    if (fieldData.format && fieldData.format === 'email') return 'email';
    if (fieldData.format && fieldData.format === 'password') return 'password';

    return 'string';
  };

  return <Input
    {...fieldData}
    name={field}
    type={getInputType()}
    label={fieldData.required && {icon: 'asterisk'}}
    labelPosition='right corner'
  />
};

BasicStringInput.propTypes = {
  fieldData: PropTypes.object.isRequired,
  field: PropTypes.string.isRequired
};


export default BasicStringInput;
