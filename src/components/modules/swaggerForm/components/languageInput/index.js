import React, {useState} from "react";
import {Form} from "semantic-ui-react";
import PropTypes from 'prop-types';
import getLanguage from "../../../../../utils/getLanguage";
import './index.scss';
import LanguageInputModal from "./components/languageInputModal";


/**
 * LanguageInput component
 * @file Use this component with required the properties to render an input that stores multiple languages
 */

/**
 *
 * @param {Array} languages - the array of languages which contain the languageId and the languageCode
 * @param {String} property - the name of the field which contains the data of the meta object, 'text' by default
 * @param {Object} meta - the object that contains the language id and code, and the data
 * @returns {React.Component} - Form.Field input wrapper
 * @memberOf module:SwaggerForm
 * @constructor
 */
const LanguageInput = ({
                         languages = [
                           {
                             languageId: 1,
                             languageCode: "en"
                           },
                           {
                             languageId: 2,
                             languageCode: "hu"
                           },
                           {
                             languageId: 3,
                             languageCode: "de"
                           },
                           {
                             languageId: 4,
                             languageCode: "es"
                           },
                           {
                             languageId: 5,
                             languageCode: "zh"
                           },
                           {
                             languageId: 6,
                             languageCode: "ab"
                           },
                           {
                             languageId: 7,
                             languageCode: "ar"
                           }
                         ],
                         property = 'text',
                         meta = []
                       }) => {

  const [metas, setMetas] = useState(meta);
  const [englishText, setEnglishText] = useState(getLanguage(meta, property));
  const [open, setOpen] = useState(false);

  /**
   * handleChange - Saves the changes into the metas state
   * @param {Object} event - The event that fires when one of the inputs change
   * @param {Object} language - Language object that includes the languageId and the languageCode
   * @method
   */
  const handleChange = (event, language) => {
    const value = event.target.value;
    let temp = [...metas];
    if (temp.map(lang => lang.languageCode).includes(language.languageCode)) {
      for (let i in temp) {
        if (temp[i].languageCode === language.languageCode) {
          temp[i][property] = value;
          break;
        }
      }
      setMetas(temp);
      setEnglishText(getLanguage(temp, property));
    } else {
      const newMeta = [...temp, {
        languageId: languages.find(l => l.languageCode === language.languageCode).languageId,
        languageCode: language.languageCode,
        [property]: value
      }];
      setMetas(newMeta);
      setEnglishText(getLanguage(newMeta, property));
    }
  };
  /**
   * @todo
   * saveChanges - This will return the metas state to the parent component, it only closes the modal right now
   * @method
   */
  const saveChanges = () => {
    // eslint-disable-next-line
    console.log(metas);
    setOpen(false);
  };

  return <Form.Field>
      <label>Input language component</label>
      <Form.Input
        placeholder='The default language is english'
        name="inputLanguage_0"
        value={englishText}
        icon={{
          name: 'language', link: true, onClick: () => {
            setOpen(true)
          }, size: 'large'
        }}
        onChange={event => {
          handleChange(event, {languageCode: 'en', languageId: 1});
          setEnglishText(event.target.value);
        }}
      />
      <LanguageInputModal
        languages={languages}
        property={property}
        metas={metas}
        open={open}
        setOpen={setOpen}
        saveChanges={saveChanges}
        handleChange={handleChange}
      />
    </Form.Field>


};

LanguageInput.propTypes = {
  languages: PropTypes.array,
  property: PropTypes.string,
  meta: PropTypes.object
};

export default LanguageInput;
