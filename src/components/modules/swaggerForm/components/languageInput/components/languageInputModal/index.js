import React from "react";
import {Form, Button, Modal, Icon} from "semantic-ui-react";
import PropTypes from 'prop-types';
import ISO6391 from 'iso-639-1';

/**
 * LanguageInputModal component
 * @file It's a component of the <LanguageInput/> component
 * @module SwaggerForm/LanguageInput/components/LanguageInputModal
 */

/**
 * @param {Array} languages - the array of languages which contain the languageId and the languageCode
 * @param {String} property - the name of the field which contains the data of the meta object, 'text' by default
 * @param {Array} metas - the array of metas that come from the parent component
 * @param {Boolean} open - it determines if the modal is closed
 * @param {Function} setOpen - it sets the open state in the parent component
 * @param {Function} saveChanges - saves the changes into the Form element
 * @param {Function} handleChange - saves the changes into the metas array
 * @returns {React.Component} - Modal component
 * @constructor
 */
const LanguageInputModal = ({
                              languages,
                              property,
                              metas,
                              open,
                              setOpen,
                              saveChanges,
                              handleChange
                            }) => {

  return <Modal
    open={open} centered={false}
    closeOnEscape={true}
    closeOnDimmerClick={true}
    onClose={() => {
      setOpen(false)
    }}
    closeIcon
  >
    <Modal.Header>Edit in more languages</Modal.Header>
    <Modal.Content>
      <Form>
        {languages &&
        languages.map((language, index) => {
          const value = metas.find(
            text => text.languageCode === language.languageCode
          );

          return (
            <Form.Field key={index}>
              <label className="native-language-name">{ISO6391.getNativeName(language.languageCode)}</label>
              <Form.Input
                value={value ? value[property] : ''}
                onChange={event => handleChange(event, language)}
              />
            </Form.Field>
          );
        })}
        <Button animated='fade' onClick={saveChanges}>
          <Button.Content hidden>Save</Button.Content>
          <Button.Content visible>
            <Icon name='save'/>
          </Button.Content>
        </Button>
      </Form>
    </Modal.Content>
  </Modal>;

};

LanguageInputModal.propTypes = {
  languages: PropTypes.array,
  property: PropTypes.string,
  metas: PropTypes.array,
  open: PropTypes.bool,
  setOpen: PropTypes.func,
  saveChanges: PropTypes.func,
  handleChange: PropTypes.func
};

export default LanguageInputModal;
