import React, {useState, useEffect} from 'react';
import PropTypes from "prop-types";
import {Form} from "semantic-ui-react";
import RequestError from "../requestError";
import {getInput} from "../../../utils/forms";
/**
 * Dynamic Swagger based form component
 * @file Use this component with required properties for render advanced forms from swagger definitions
 * @module SwaggerForm
 */

/**
 * Use this component with required properties for render advanced forms from swagger definitions
 * @param {Function|Boolean} request - Swagger request function (Or networking layer request function)
 * @param {Object} data - Give a data object where key is the field name
 * @param {String} definition - Enter a swagger definition here to fetch data from
 * @return {React.Component} Form
 * @methodOf SwaggerForm
 * @constructor
 */
const SwaggerForm = ({request = false, data = {}, definition = ''}) => {
  const [error, ] = useState(false);
  const [propertiesObject, setPropertiesObject] = useState(false);
  const [requiredProperties, setRequiredProperties] = useState(false);

  useEffect(() => {
    setPropertiesObject(window.swaggerClient.spec.components.schemas[definition].properties);
    setRequiredProperties(window.swaggerClient.spec.components.schemas[definition].required);
  }, [window.swaggerClient]);

  if(request) useEffect(() => {}, [window.swaggerClient]);

  if (error) return <RequestError error={error}/>;

  return <Form id={definition + '_swaggerForm'}>
    {requiredProperties && propertiesObject && Object.keys(propertiesObject).map(property =>
      getInput(
        propertiesObject[property] || {},
        property || '',
        data[property] || '',
        requiredProperties.includes(property) || false
      )
    )}
  </Form>;
};

SwaggerForm.propTypes = {
  definition: PropTypes.string.isRequired,
  data: PropTypes.object,
  request: PropTypes.func
};

export default SwaggerForm;
