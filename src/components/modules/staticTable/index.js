/**
 * Dynamic table component
 * @file Use this component with required properties for render advanced tables
 * @module StaticTable
 */
import React from 'react';
import PropTypes from "prop-types";
import {Table} from "semantic-ui-react";
import * as tables from "../../../utils/tables";

const StaticTable = ({data = [], headers = {}}) => {
  return <Table striped>
    <Table.Header>
      {tables.renderTableHeaderRow(headers)}
    </Table.Header>

    <Table.Body>
      {tables.renderTableBodyRows(headers, data)}
    </Table.Body>
  </Table>;
};

StaticTable.propTypes = {
  headers: PropTypes.object.isRequired,
  data: PropTypes.array.isRequired
};

export default StaticTable;
