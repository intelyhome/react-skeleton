/**
 * Basic request error component
 * @file Use this component with swaggerClient.req.catch(errorResponse) object
 * @module RequestError
 */
import React from 'react';
import {Icon, Message} from "semantic-ui-react";
import PropTypes from "prop-types";

const RequestError = ({error = {}}) => {
  return <Message icon size={'tiny'} error>

    {error.statusCode === 401 && <Icon name='lock notched'/>}

    <Message.Content>
      <Message.Header>Could not process your request...</Message.Header>
      {error.response && error.response.body && error.response.body.message || 'Error code: '+ error.statusCode}
    </Message.Content>
  </Message>;
};

RequestError.propTypes = {
  error: PropTypes.object.isRequired
};

export default RequestError;
