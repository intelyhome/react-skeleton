/**
 * Basic Page header component
 * @file Use this component for default pageHeader
 * @module PageHeader
 */
import React from 'react';
import {Divider, Grid, Header, Icon} from "semantic-ui-react";
import PropTypes from "prop-types";

const PageHeader = ({children = null, page = 'Homepage', icon = 'home', altText = ''}) => {
  return <Grid>
      <Grid.Column container={true.toString()}>
        <Header as='h2'>
        <div className={'pull-right'}>{children}</div>
        <Icon name={icon}/>
        <Header.Content>
          {page}
          <Header.Subheader>{altText}</Header.Subheader>
        </Header.Content>
        <Divider/>
      </Header>
      </Grid.Column></Grid>;
};

PageHeader.propTypes = {
  page: PropTypes.string,
  altText: PropTypes.string,
  icon: PropTypes.string,
  children: PropTypes.any
};

export default PageHeader;
