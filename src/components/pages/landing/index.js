import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from 'react-redux';
import * as actions from '../../../actions/userActions';
import {Container, Placeholder} from "semantic-ui-react";

const LandingHome = () => {

  const loading = <Placeholder>
      <Placeholder.Header image>
        <Placeholder.Line/>
        <Placeholder.Line/>
      </Placeholder.Header>
      <Placeholder.Paragraph>
        <Placeholder.Line length='medium'/>
        <Placeholder.Line length='short'/>
      </Placeholder.Paragraph>
    </Placeholder>;
  return (
      <Container className={'page-container'}>
        {loading}
        {loading}
        {loading}
        {loading}
        {loading}
        {loading}
        {loading}
        {loading}
        {loading}
        {loading}
        {loading}
        {loading}
        {loading}
        {loading}
        {loading}
        {loading}
        {loading}
    </Container>
  );
};

function mapStateToProps(state) {
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LandingHome);
