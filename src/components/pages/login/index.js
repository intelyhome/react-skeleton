import React, {useState} from 'react';
import {Link, Redirect} from "react-router-dom";
import {bindActionCreators} from "redux";
import authActions from "../../../actions/authActions";
import userActions from "../../../actions/userActions";
import {connect} from "react-redux";
import {Button, Card, Form, Grid, Icon, Message, Responsive} from "semantic-ui-react";
import {pages as PagesConfig} from "../../../config/pages";
import PropTypes from "prop-types";

const LoginIndex = ({authReducer, authActions}) => {
  const [loading, setLoading] = useState(false);

  const setLoadingFalse = () => setLoading(false);

  const submitForm = event => {
    setLoading(true);
    logInAction(getFormData(event))
  };
  const getFormData = event => {
    return {'email': event.target.email.value, 'password': event.target.password.value}
  };
  const logInAction = formData => authActions.signInAction(formData.email, formData.password).finally(setLoadingFalse);

  const loginForm = () => {
    return (
      <Form onSubmit={submitForm} id={'loginForm'} error={!!authReducer.error}>
        <Message
          error
          header='Failed to process your request'
          content='You have entered invalid e-mail address or password'
        />
        <Form.Field>
          <label className={'text-left'} htmlFor={'LoginEmail'}>E-mail</label>
          <input placeholder={'example@mail.com'} id={'LoginEmail'} name={'email'} type={'email'} disabled={loading}/>
        </Form.Field>
        <Form.Field>
          <label className={'text-left'} htmlFor={'LoginPassword'}>Password</label>
          <input placeholder={'******'} id={'LoginPassword'} name={'password'} type={'password'} disabled={loading}/>
        </Form.Field>
        <Button type='submit' animated={'vertical'} fluid loading={loading}>
          <Button.Content visible>Log in</Button.Content>
          <Button.Content hidden>
            <Icon name='stethoscope'/>
          </Button.Content>
        </Button>
      </Form>);
  };

  if (authReducer.token != null) return <Redirect to={PagesConfig.Home.link}/>;

  return <Card fluid>
    <Card.Content>
      <Card.Header className={'logo-wrapper'}>
        Log in
        <Responsive minWidth={600}><p className={'logo-subtext'}>Lorem ipsum dolor sit amet, <strong>consectetur adipiscing elit.</strong> Vestibulum ac sem neque. Vivamus suscipit feugiat
          tristique. Suspendisse interdum <strong>felis vitae</strong> ullamcorper
          consectetur.</p></Responsive>
      </Card.Header>
      {loginForm()}
    </Card.Content>
    <Card.Content extra>

      <Grid padded={true} stretched doubling columns={2}>
          <Grid.Column>
            <Button fluid color='facebook'>
              <Icon name='facebook'/> Facebook
            </Button>
          </Grid.Column>
          <Grid.Column>
            <Button fluid color='linkedin'>
              <Icon name='linkedin'/> LinkedIn
            </Button>
          </Grid.Column>
      </Grid>
    </Card.Content>
    <Card.Content extra>
      <Link to={"#" + PagesConfig.ForgotPassword.link}>
        Forgot your password?
      </Link>
      <Link to={"#" + PagesConfig.ForgotPassword.link} className={'pull-right'}>
        Sign up
      </Link>
    </Card.Content>
  </Card>
};

function mapStateToProps(state) {
  return {
    authReducer: state.authReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
    userActions: bindActionCreators(userActions, dispatch)
  };
}


LoginIndex.propTypes = {
  authActions: PropTypes.object,
  authReducer: PropTypes.object
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginIndex);
