import React from 'react';
import {Link} from 'react-router-dom';
import {bindActionCreators} from "redux";
import {connect} from 'react-redux';
import * as actions from '../../../actions/userActions';
import {
  Button,
  Container,
  Divider,
  Feed,
  Form,
  Grid,
  Icon,
  Label,
  Message,
  Placeholder,
  Segment
} from "semantic-ui-react";
import PageHeader from "../../modules/pageHeader";
import LanguageInput from "../../modules/swaggerForm/components/languageInput";

const HomeIndex = () => {

  const loading = <Placeholder>
    <Placeholder.Header image>
      <Placeholder.Line/>
      <Placeholder.Line/>
    </Placeholder.Header>
    <Placeholder.Paragraph>
      <Placeholder.Line length='medium'/>
      <Placeholder.Line length='short'/>
    </Placeholder.Paragraph>
  </Placeholder>;


  const image = 'https://randomuser.me/api/portraits/men/76.jpg';
  const date = '3 days ago';
  const summary = 'You added Jenny Hess to your coworker group.';
  return (
    <Container>
      <PageHeader page={'Home'} altText={'Manage your preferences'}>
        <Button
          content='Settings'
          fitted
          icon='cog'
          label={{as: 'a', basic: true, pointing: 'right', content: '2,048'}}
          labelPosition='left'
        />
      </PageHeader>

      <Message icon size={'tiny'}>
        <Icon name='circle notched' loading/>
        <Message.Content>
          <Message.Header>Just one second</Message.Header>
          We are almost ready...
        </Message.Content>
      </Message>

      <Grid>
        <Grid.Row columns={2} divided>
          <Grid.Column width={11}>
            <Segment>
              <Label as='a' color='red' ribbon={'right'}>
                Overview
              </Label>
              {loading}
              {loading}
              {loading}
            </Segment>
          </Grid.Column>
          <Grid.Column width={5}>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis volutpat mauris, a tempor libero.
              Aliquam sapien ligula, dictum quis elit sit amet, egestas vestibulum augue.
            </p>

            <Divider/>

            <Feed>
              <Feed.Event image={image} date={date} summary={summary}/>

              <Feed.Event>
                <Feed.Label image={image}/>
                <Feed.Content date={date} summary={summary}/>
              </Feed.Event>
            </Feed>

          </Grid.Column>
        </Grid.Row>
      </Grid>
      <p>
        <Link to="/other">Click this link</Link> to jump other page.
      </p>
      <Grid className={'grid-holder'}>
        <Grid.Row>
          <Grid.Column>
            <Form>
              <LanguageInput/>
            </Form>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  );
};

function mapStateToProps(state) {
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeIndex);
