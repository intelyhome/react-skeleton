import React from 'react';
import PageHeader from "../../modules/pageHeader";
import {Button, Container, Grid} from "semantic-ui-react";
import {useTranslation} from 'react-i18next';
import {Link} from "react-router-dom";
import SwaggerForm from "../../modules/swaggerForm";

const UIKitForm = () => {
  const {t} = useTranslation('uikit');

  return <Container>
    <PageHeader page={t('form.title')} icon={'columns'} altText={t('form.title-alt')}>
      <Button
        as={Link}
        to={'/ui-kit/table'}
        primary
        content={t('uikit:form.saveEntryButton')}
        fitted={true.toString()}
      />
    </PageHeader>
    <Grid className={'grid-holder'}>
      <Grid.Row>
      <Grid.Column>
        <SwaggerForm definition={'User'} data={{firstname: 'Ferenc', lastname: 'Farkas', 'email': 'ferenc.farkas@drukka.hu'}}/>
      </Grid.Column>
      </Grid.Row>
    </Grid>
  </Container>;
};

export default UIKitForm;
