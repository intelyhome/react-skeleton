import React from 'react';
import SwaggerTable from "../../modules/swaggerTable";
import PageHeader from "../../modules/pageHeader";
import {Button, Container, Icon} from "semantic-ui-react";
import {useTranslation} from 'react-i18next';
import UsersActions from "../../../actions/usersActions";
import {Link} from "react-router-dom";

const UIKitTable = () => {
  const {t} = useTranslation('uikit');

  const customControlScript = rowId => {
    confirm(t('common:confirmations.beforeRemove', {entry: rowId}));
    return false;
  };

  return <Container>
    <PageHeader page={t('table.title')} icon={'table'} altText={t('table.title-alt')}>
      <Button
        as={Link}
        to={'/ui-kit/form'}
        content={t('table.newEntryButton')}
        fitted={true.toString()}
      />
    </PageHeader>
    <SwaggerTable
      columns={{
        firstname: t('table.headers.firstname'),
        lastname: t('table.headers.lastname'),
        email: t('table.headers.email'),
        testModelProperty: t('table.headers.testModelProperty')
      }}
      controls={[
        <Icon key='edit' name={'pencil'}/>,
        <Icon key='remove' name={'trash'}
              customScript={customControlScript}
        />
      ]}
      request={UsersActions.listUsers}/></Container>;
};

export default UIKitTable;
