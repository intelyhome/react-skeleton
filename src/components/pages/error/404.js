import React from 'react';
import {Container, Grid, Image} from "semantic-ui-react";
import FallenRocket from "../../../assets/vectors/notfound-rocket-fallen.svg";
import SVG from 'react-inlinesvg';
import './404.scss';

// Since this component is simple and static, there's no parent container for it.
const Error404 = () => {
  return (
  <Container className={'page-container error404'}>
    <Grid columns={2}>
      <Grid.Row>
        <Grid.Column textAlign={'center'} tablet={16} computer={8}>
          <Image src='https://react.semantic-ui.com/images/wireframe/media-paragraph.png'/>
          <Image src='https://react.semantic-ui.com/images/wireframe/media-paragraph.png'/>
        </Grid.Column>
        <Grid.Column stretched textAlign={'center'} verticalAlign={'bottom'} tablet={16} computer={8} style={{borderBottom: '2px solid #392b2b'}}>
          <SVG src={FallenRocket} width={'100%'}/>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </Container>
  );
};

export default Error404;
