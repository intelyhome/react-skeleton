const usersNetworking = {
  listUsers: () => {
    return window.swaggerClient.apis.user.listUsers();
  }
};

export default usersNetworking;
