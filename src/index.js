/* eslint-disable import/default */
import React from 'react';
import {render} from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import configureStore, {history} from './store/configureStore';
import Loadable from "react-loadable";
import Loader from './components/modules/loader';

import './favicon.ico';
import 'babel-polyfill';

/**
 * @file src/index.js - Start everything from here
 * @author Ferenc Farkas - Drukka Digitals
 * @see <a href="https://drukkadigitals.com">www.drukkadigitals.com</a> for more
 */

const store = configureStore();

const ErrorBoundary = Loadable({
  loader: () => import('./components/modules/errorBoundary' /* webpackChunkName: "errorboundary" */),
  loading: Loader
});

const App = Loadable({
  loader: () => import('./components/App' /* webpackChunkName: "App", webpackPrefetch: true */),
  loading: Loader
});


window.debug = {
  // eslint-disable-next-line no-console
  log: (title, message) => console.log(message ? title : message, message && message),
  // eslint-disable-next-line no-console
  error: (title, message) => console.error(message ? title : message, message && message),
  // eslint-disable-next-line no-console
  table: ( data ) => console.table(data)
};

if ('serviceWorker' in window.navigator) {
  window.navigator.serviceWorker
    .register('/service-worker.js')
    .then(function () {
      window.debug.log("SW:","Service Worker Registered");
    });

  window.navigator.serviceWorker.ready.then(function () {
    window.debug.log('SW:', 'Service Worker Ready');
  });
}

render(
  <AppContainer>
    <ErrorBoundary>
      <App store={store} history={history}/>
    </ErrorBoundary>
  </AppContainer>,
  document.getElementById('app')
);

if (module.hot) {
  module.hot.accept('./components/App', () => {
    const NewRoot = require('./components/App').App;
    render(
      <AppContainer>
        <ErrorBoundary>
          <NewRoot store={store} history={history}/>
        </ErrorBoundary>
      </AppContainer>,
      document.getElementById('app')
    );
  });
}
