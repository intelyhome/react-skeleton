import usersNetworking from "../networking/usersNetworking";
import User from "../models/User.model";

const usersActions = {
  listUsers: async () => {
    return usersNetworking.listUsers()
      .then(response => !response.ok || !response.body ? false : response) // validate response
      .then(response => response.body.map(userRaw => User.parse(userRaw))) // Parse response to model
  }
};

export default usersActions;
