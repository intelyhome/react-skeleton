export default Object.freeze({
  API: {
    host: 'https://development.api.skeleton.proxy.drukka.hu/api-docs',
    headers: {
      Authentication: {
        name: 'Authorization',
        localStorageKey: 'AuthenticationToken',
        prefix: 'Bearer '
      }
    },
    params: {
      list: {
        itemsPerPage: 5
      }
    }
  },
  Localize: {
    defaultLanguageCode: 'en'
  },
  App: {
    name: 'Cancell'
  }
});
