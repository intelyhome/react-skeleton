import Loadable from "react-loadable";

import Loader from '../../components/modules/loader';

const LandingLayout = Loadable({
  loader: () => import('./../../components/layouts/landingLayout/Layout' /* webpackChunkName: "layout-landingLayout", webpackPrefetch: true */),
  loading: Loader
});

export default {
  Landing: {
    title: 'landing.title',
    link: '/',
    exact: true,
    layout: LandingLayout,
    component: Loadable({
      loader: () => import('../../components/pages/error/404' /* webpackChunkName: "pages-error-404" */),
      loading: Loader
    })
  }
}
