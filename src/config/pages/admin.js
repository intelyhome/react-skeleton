import Loadable from "react-loadable";
import Loader from '../../components/modules/loader';

const AdminLayout = Loadable({
  loader: () => import('./../../components/layouts/adminLayout/Layout' /* webpackChunkName: "layout-adminLayout", webpackPrefetch: true */),
  loading: Loader
});

export default {
  Home: {
    title: 'home.title',
    link: '/dashboard/home',
    exact: true,
    layout: AdminLayout,
    authenticationRequired: true,
    component: Loadable({
      loader: () => import('../../components/pages/home' /* webpackChunkName: "pages-home-index" */),
      loading: Loader
    })
  },
  UIKitTable: {
    title: 'home.title',
    link: '/dashboard/ui-kit/table',
    exact: true,
    layout: AdminLayout,
    authenticationRequired: true,
    component: Loadable({
      loader: () => import('../../components/pages/ui-kit/table' /* webpackChunkName: "pages.ui-kit.table" */),
      loading: Loader
    })
  },
  UIKitForm: {
    title: 'home.title',
    link: '/dashboard/ui-kit/form',
    exact: true,
    layout: AdminLayout,
    authenticationRequired: true,
    component: Loadable({
      loader: () => import('../../components/pages/ui-kit/form' /* webpackChunkName: "pages.ui-kit.form" */),
      loading: Loader
    })
  }
}
