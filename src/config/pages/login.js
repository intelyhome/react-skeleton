import Loadable from "react-loadable";
import Loader from '../../components/modules/loader';

const AuthenticationLayout = Loadable({
  loader: () => import('./../../components/layouts/authenticationLayout/Layout' /* webpackChunkName: "layout-authenticationLayout", webpackPrefetch: true */),
  loading: Loader
});

export default {
  Login: {
    title: 'login.title',
    link: '/dashboard/signin',
    exact: true,
    layout: AuthenticationLayout,
    component: Loadable({
      loader: () => import('../../components/pages/login' /* webpackChunkName: "pages-login-index" */),
      loading: Loader,
      delay: 5500
    })
  },
  Signup: {
    title: 'signup.title',
    link: '/dashboard/signup',
    exact: true,
    layout: AuthenticationLayout,
    component: Loadable({
      loader: () => import('../../components/pages/login/signup' /* webpackChunkName: "pages-login-signup" */),
      loading: Loader
    })
  },
  ForgotPassword: {
    title: 'forgotpassword.title',
    link: '/dashboard/forgot-password',
    exact: true,
    layout: AuthenticationLayout,
    component: Loadable({
      loader: () => import('../../components/pages/login/signup' /* webpackChunkName: "pages-login-signup" */),
      loading: Loader
    })
  }
}
