/* use strict */
import loginPageGroup from './pages/login';
import landingPageGroup from './pages/landing';
import adminPageGroup from './pages/admin';
import Loadable from "react-loadable";
import Loader from "../components/modules/loader";

const LandingLayout = Loadable({
  loader: () => import('./../components/layouts/landingLayout/Layout' /* webpackChunkName: "layout-landingLayout", webpackPrefetch: true */),
  loading: Loader
});

export const pages = Object.freeze({
  ...landingPageGroup,
  ...loginPageGroup,
  ...adminPageGroup,

  Error404: {
    link: '*',
    layout: LandingLayout,
    component: Loadable({
      loader: () => import('../components/pages/error/404' /* webpackChunkName: "pages-error-404" */),
      loading: Loader
    })
  }
});
