import React from 'react';
import {Form} from "semantic-ui-react";
import BasicStringInput from "../components/modules/swaggerForm/components/basicStringInput";
import PropTypes from "prop-types";
/**
 * Basic form utils and helpers
 *
 * @namespace forms
 * @memberOf utils
 */


/**
 * This helper give you back a form element by type
 * @example
 * return getInput({type: "string"}, "firstname", "Ferenc", true)
 * @param {Object} fieldData - Pass the Swagger object for property
 * @param {String} field - Name the field (like firstname)
 * @param {String} value - Set defaultValue as this
 * @param {Boolean} isRequired - This field is required
 * @function getInput
 * @memberOf utils.forms
 * @return {React.Component} RenderFormField
 */
export const getInput = (fieldData = {}, field = '', value = '', isRequired = false) => {
  fieldData['required'] = isRequired;
  fieldData['defaultValue'] = value;
  if(fieldData && fieldData.type && fieldData.type.includes('string')) {
    return <RenderFormField fieldData={fieldData} field={field}/>;
  }
};


/**
 * RenderFormField - This component render Form.Field elements by type
 * @param {Object} fieldData - Pass the Swagger object for property
 * @param {String} field - Name the field (like firstname)
 * @constructor
 * @memberOf utils.forms
 * @return {React.Component} Form.Field
 */
const RenderFormField = ({fieldData = {}, field = ''}) => {
  return <Form.Field key={field}>
    <label>{field}</label>
    <BasicStringInput fieldData={fieldData} field={field}/>
  </Form.Field>;
};

RenderFormField.propTypes = {
  fieldData: PropTypes.object.isRequired,
  field: PropTypes.string.isRequired,
};
