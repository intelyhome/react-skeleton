import React from 'react';
import {Table} from "semantic-ui-react";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
/**
 * Basic table utils and helpers
 *
 * @namespace tables
 * @memberOf utils
 */

/**
 * Render table headers
 * @param {Object} headers - Headers object
 * @param {Boolean|Node[]} controls - Enable control item(s)
 * @function renderTableHeaderRow
 * @memberOf utils.tables
 * @return {React.Component} Table.Row
 */
export const renderTableHeaderRow = (headers, controls = false) => <Table.Row>
  {Object.keys(headers).map(header => <Table.HeaderCell
    key={'tableHeader_cell_' + header}>{headers[header]}</Table.HeaderCell>)}
  {controls && <Table.HeaderCell key={'tableHeader_cell_controls'}/>}
</Table.Row>;


/**
 * Render table body rows from array
 * @param {Object} headers - Headers object
 * @param {Object[]} data - Data to render in Table
 * @param {Boolean|Node[]} controls - Enable control item(s)
 * @function renderTableBodyRows
 * @memberOf utils.tables
 * @return {React.Component[]} Table.Row
 */
export const renderTableBodyRows = (headers, data, controls = false) => data.map(row => <Table.Row
  key={'tableBody_row_' + row.id}>
  {Object.keys(headers).map(header => <Table.Cell
    key={'tableBody_cell_' + row.id + '_' + header}>{row[header]}</Table.Cell>)}
  {controls && renderTableControls(row, controls)}
</Table.Row>);

/**
 * Render Table control items
 * @param {Object} row - Data of current row
 * @param {Node[]} controls - Array of control items
 * @function renderTableControls
 * @memberOf utils.tables
 * @return {React.Component} Table.Cell
 */
export const renderTableControls = (row, controls) => <Table.Cell key={'tableBody_cell_' + row.id + '_' + 'controls'}
                                                                  textAlign={'right'}>
  {controls.map(control => {
    const controlKey = control.key;
    return <RenderControlItem key={row.id + 'ControlItem' + controlKey} ending={controlKey}
                              row={row}>{control}</RenderControlItem>
  })}
</Table.Cell>;


/**
 * RenderFormField - This component render Form.Field elements by type
 * @param {Object} row - Data of current row
 * @param {String} ending - Set path for control link (like xxx/rowId/edit)
 * @param {Node} children - Children component if controller
 * @constructor
 * @memberOf utils.tables
 * @return {React.Component} Link
 */
const RenderControlItem = ({row, ending, children}) => {
    const {customScript} = children.props;

    const onClickHandler = event => {
      event.preventDefault();
      customScript(row.id)
    };

    return <Link onClick={customScript && onClickHandler}
                 to={window.location.pathname + '/' + row.id + '/' + ending}>{children}</Link>
};

RenderControlItem.propTypes = {
  row: PropTypes.object.isRequired,
  ending: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
};
