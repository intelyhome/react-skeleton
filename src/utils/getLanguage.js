// the language with the first id (english)
const languageId = 1;
const getLanguage = (metaList = [], property='text') => {
  if (metaList.length === 0) return '';
  if (metaList.length === 1 && metaList[0].languageId !== languageId) return '';
  return !Array.isArray(metaList) ?
    metaList[property] : metaList.find(meta => meta.languageId === languageId)[property] || metaList[0][property] || "";
};
export default getLanguage;
