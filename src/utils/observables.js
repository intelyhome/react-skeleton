/**
 * Observable module
 * @file Use this module to register and call event listeners global in the App
 * @see {@link Observables#register|Observables.register} to register a new Observable
 * @see {@link Observables#call|Observables.call} to call a registered Observable
 * @module Observables
 */
/**
 * @typedef ObservableObject
 * @type {Object}
 * @property {string} name - Name of the event
 * @property {function} callback - The callback function
 */

/**
 * @constant
 * @type {ObservableObject[]}
 */
const observers = [];

const Observables = {
  /**
   * Find index in observers array
   * @methodOf Observables
   * @function findIndex
   * @memberOf Observables
   * @param {String} name - Name of observable
   * @return {number} index - Index in Array
   */
  findIndex: (name) => observers.findIndex(observer => observer.name === name),

  /**
   * Find observable object in observers array
   * @methodOf Observables
   * @function find
   * @param name
   * @return {ObservableObject} Observable - Return the full object of Observable
   */
  find: (name) => observers.find(observer => observer.name === name),

  /**
   * Register new observable eventListener
   * @methodOf Observables
   * @function register
   * @param {String} name - Name of observable
   * @param {Function|Undefined} callback - Register a callback function for this observable
   * @return {boolean} return - Return the status of registration
   */
  register: (name, callback = () => {
  }) => {
    if (Observables.findIndex(name)) {
      window.debug.log('Observable registered', {name: name, callback: callback});
      observers.push({
        name: name,
        callback: callback
      });
      return true;
    }
    return false;
  },

  /**
   * Remove an observable from observers array
   * @methodOf Observables
   * @function remove
   * @param {String} name - Name of observable
   * @return {Boolean} return - Return the status of remove
   */
  remove: (name) => {
    const findByName = Observables.findIndex(name);
    if (findByName) {
      observers.slice(findByName, 1);
      return true;
    }

    return false;
  },

  /**
   * Call the observable
   * @methodOf Observables
   * @function call
   * @param {String} name - Name of observable
   * @param {*} data - The data to fire to the observable
   * @return {Boolean} return - Return the status of call
   */
  call: (name, data) => {
    const observable = Observables.find(name);
    if (observable) {
      window.debug.log('Observable called', {name: name, data: data});
      observable.callback(data);
      return true;
    }
    return false;
  }
};

export default Observables;
