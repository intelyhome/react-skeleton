import Model from "js-model";

let ErrorResponseBody = new Model({
    message: {
      type: String,
      default: 'Unexpected error'
    }
});
let ErrorResponseResponse = new Model({
  ok: {
    type: Boolean,
    default: false
  }
});

let ErrorResponse = new Model({
  status: Number,
  statusCode: Number,
  response: ErrorResponseResponse,
  body: ErrorResponseBody
});


export default ErrorResponse;
