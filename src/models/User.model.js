import Model from "js-model";

let User = new Model({
  id: 0,
  email: String,
  firstname: '',
  lastname: '',
  activated: false,
  testModelProperty: {
    type: String,
    default: Math.random()
  },
  profilePicture: {
    type: String,
    default: 'https://randomuser.me/api/portraits/women/72.jpg'
  }
});

export default User;
